﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV3_RPPOON
{
    class NotificationDirector
    {
        private IBuilder notificationDirector;

        public NotificationDirector(IBuilder Builder)
        {
            notificationDirector = Builder;
        }

        public ConsoleNotification CreateInfoNotification(string Author)
        {
            return notificationDirector.SetAuthor(Author).SetColor(ConsoleColor.Green).SetTime(DateTime.Now).SetTitle("INFO").SetText("Not important").Build();
        }

        public ConsoleNotification CreateAlertNotification(string Author)
        {
            return notificationDirector.SetAuthor(Author).SetColor(ConsoleColor.Yellow).SetTime(DateTime.Now).SetTitle("ALERT").SetText("Less important").Build();
        }

        public ConsoleNotification CreateErrorNotification(string Author)
        {
            return notificationDirector.SetAuthor(Author).SetColor(ConsoleColor.Red).SetTime(DateTime.Now).SetTitle("ERROR").SetText("Important").Build();
        }

    }
}
