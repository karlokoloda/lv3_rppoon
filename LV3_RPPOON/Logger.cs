﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV3_RPPOON
{
    class Logger
    {
        private static Logger instance;
        string filePath;

        private Logger()
        {
            this.filePath = @"..\..\Log1.txt";
        }
        public static Logger GetInstance()
        {
            if (instance == null)
            {
                instance = new Logger();
            }
            return instance;
        }

        public void Log(string log)
        {
            using (System.IO.StreamWriter writer = new System.IO.StreamWriter(this.filePath, true))
            {
                writer.WriteLine(log);
            }

        }
        public void setFilePath(string filePath)
        {
            this.filePath = filePath;
        }

    }

}
