﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV3_RPPOON
{
    class RandomMatrixGenerator
    {
        private static RandomMatrixGenerator instance;
        private RandomGenerator randomGenerator;

        private RandomMatrixGenerator()
        {
            this.randomGenerator = RandomGenerator.GetInstance();
        }
        public static RandomMatrixGenerator GetInstance()
        {
            if (instance == null)
            {
                instance = new RandomMatrixGenerator();
            }
            return instance;
        }

        public double[][] generateMatrix(int a, int b)
        {
            double[][] matrix = new double[a][];

            for (int i = 0; i < a; i++)
            {
                matrix[i] = new double[b];
                for (int j = 0; j < b; j++)
                {
                    matrix[i][j] = randomGenerator.NextDouble();
                }
            }
            return matrix;
        }

    }
}
