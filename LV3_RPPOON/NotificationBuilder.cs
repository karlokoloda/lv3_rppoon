﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV3_RPPOON
{
    class NotificationBuilder : IBuilder
    {
        public string Author = "";
        public string Title = "";
        public string Text = "";
        public DateTime Timestamp = DateTime.Now;
        public Category Level = Category.INFO;
        public ConsoleColor Color = ConsoleColor.Green;

        public ConsoleNotification Build()
        {
            return new ConsoleNotification(Author, Title, Text, Timestamp, Level, Color);
        }

        public IBuilder SetAuthor(string author)
        {
            this.Author = author;
            return this;
        }

        public IBuilder SetColor(ConsoleColor color)
        {
            
            this.Color = color;
            return this;
        }

        public IBuilder SetLevel(Category level)
        {
            
            this.Level = level;
            return this;
        }

        public IBuilder SetText(string text)
        {
            
            this.Text = text;
            return this;
        }

        public IBuilder SetTime(DateTime time)
        {
            
            this.Timestamp = time;
            return this;
        }

        public IBuilder SetTitle(string title)
        {
            
            this.Title = title;
            return this;
        }

    }
}
