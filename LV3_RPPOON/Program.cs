﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV3_RPPOON
{
    class Program
    {
        static void Main(string[] args)
        {
            //Zadatak 1. 
            string filePath = @"..\..\DataCSV1.txt";
            Dataset dataset = new Dataset(filePath);
            Dataset secondDataset = (Dataset)dataset.Clone();


            //Zadatak 2.
            RandomMatrixGenerator matrixGenerator = RandomMatrixGenerator.GetInstance();
            double[][] matrix = new double[5][];
            for (int i = 0; i < 5; i++)
            {
                matrix[i] = new double[8];
            }

            matrix = matrixGenerator.generateMatrix(5, 8);

            //Zadatak 3. 
            Logger logger = Logger.GetInstance();
            logger.Log("Successful run! Data logged.");

            //Zadatak 4. 
            ConsoleNotification consoleNotification = new ConsoleNotification("Notification", "Annoying", "Extremely", DateTime.Now, Category.ALERT, ConsoleColor.DarkMagenta);
            NotificationManager notificationManager = new NotificationManager();

            //notificationManager.Display(consoleNotification);

            //Zadatak 5.
            NotificationBuilder notificationBuilder = new NotificationBuilder();
            notificationBuilder.SetAuthor("Master Chief").SetColor(ConsoleColor.Green).SetLevel(Category.INFO);

            notificationManager.Display(consoleNotification);

            //Zadatak 6.
            NotificationDirector notificationDirector = new NotificationDirector(notificationBuilder);
            ConsoleNotification ERROR = notificationDirector.CreateErrorNotification("Master Chief");
            notificationManager.Display(ERROR);

            //Zadatak 7.
            ConsoleNotification cloneConsoleNotification = (ConsoleNotification)consoleNotification.Clone();

        }
    }
}
