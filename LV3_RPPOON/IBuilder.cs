﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV3_RPPOON
{
    interface IBuilder
    {
        ConsoleNotification Build();
        IBuilder SetAuthor(string author);
        IBuilder SetTitle(string title);
        IBuilder SetTime(DateTime time);
        IBuilder SetLevel(Category level);
        IBuilder SetColor(ConsoleColor color);
        IBuilder SetText(string text);

    }
}
